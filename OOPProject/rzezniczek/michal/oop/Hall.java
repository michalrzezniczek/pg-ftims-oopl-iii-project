/*
 * Hall.java
 * Version 1.0
 * Micha� Rze�niczek
 * Copyright (c) 2016. All rights reserved.
 */

package rzezniczek.michal.oop;

import rzezniczek.michal.oop.events.Event;
import rzezniczek.michal.oop.exceptions.EventDoesNotExistException;
import rzezniczek.michal.oop.exceptions.SeatReservedException;
import rzezniczek.michal.oop.interfaces.IPrintable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Base class which allow an application to create object similar to real hall in cinema.
 * Included information:
 * <ul>
 * <li>List of seats, events, and reserved seats.</li>
 * </ul>
 *
 * @author Micha� Rze�niczek
 * @version 1.0 2016
 */
public class Hall implements IPrintable {

    private Map<Event, Map<Integer, Boolean>> reservedSeatsOnEvent = new HashMap<>();
    private Map<Integer, Boolean> initialSeats = new HashMap<>();

    public Hall(int capacity) {
        for (int i = 1; i <= capacity; i++) {
            initialSeats.put(i, false);
        }
    }

    /**
     * Assigns an event to a hall.
     *
     * @param e <code>Event</code> object.
     */
    public void addEvent(Event e) {
        reservedSeatsOnEvent.put(e, initialSeats);
    }

    /**
     * Remove event assigned to a hall.
     *
     * @param e <code>Event</code> object.
     */
    public void delEvent(Event e) {
        reservedSeatsOnEvent.remove(e);
    }

    /**
     * Returns list of seats which are available to book on event.
     *
     * @param e <code>Event</code> object.
     * @return List with number seats which are free.
     */
    public List<Integer> getFreeSeatsOn(Event e) {
        List<Integer> result = new ArrayList<>();
        int i = 1;

        if (!reservedSeatsOnEvent.get(e).containsValue(false)) {
            return null;
        }

        for (Boolean seat : reservedSeatsOnEvent.get(e).values()) {
            if (!seat) {
                result.add(i);
            }
            i++;
        }

        return result;
    }

    /**
     * Reserves a place on a particular event.
     *
     * @param place number of seat to reserve
     * @param e     event on which you want reserve a seat
     * @throws EventDoesNotExistException
     * @throws SeatReservedException
     */
    public void bookASeat(int place, Event e)
            throws EventDoesNotExistException, SeatReservedException {
        if (place <= initialSeats.size()) {
            if (!reservedSeatsOnEvent.containsKey(e)) {
                throw new EventDoesNotExistException();
            } else if (reservedSeatsOnEvent.get(e).get(place)) {
                throw new SeatReservedException();
            } else {
                reservedSeatsOnEvent.get(e).remove(place);
                reservedSeatsOnEvent.get(e).put(place, true);
            }
        } else {
            System.out.println("Seat doesn't exist. Choose another");
        }
    }

    /**
     * Prints hall capacity.
     *
     * @return hall capacity
     */
    @Override
    public String print() {
        return "Hall's capacity = " + initialSeats.size();
    }

    @Override
    public String toString() {
        return print();
    }
}
