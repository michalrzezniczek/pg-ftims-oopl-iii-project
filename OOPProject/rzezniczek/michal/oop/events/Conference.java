/*
 * Conference.java
 * Version 1.0
 * Micha� Rze�niczek
 * Copyright (c) 2016. All rights reserved.
 */

package rzezniczek.michal.oop.events;

import rzezniczek.michal.oop.Hall;

import java.util.ArrayList;
import java.util.List;

/**
 * Conference is a class inherited from base class Event.
 * Contains:
 * <ul>
 * <li>The title</li>
 * <li>The duration (min)</li>
 * <li>Place</li>
 * <li>List of Talk</li>
 * </ul>
 *
 * @author Micha� Rze�niczek
 * @version 1.0 2016
 * @see rzezniczek.michal.oop.events.Event
 * @see rzezniczek.michal.oop.events.Talk
 */
public class Conference extends Event {
    private List<Talk> listOfTalks = new ArrayList<>();

    public Conference(String title, int duration, Hall place, List<Talk> talks) {
        super(title, duration, place);
        listOfTalks.addAll(talks);
    }

    /**
     * Returns list of Talks;
     *
     * @return List of object talk. <code>List<Talk></code>
     */
    public List<Talk> getListOfTalks() {
        return listOfTalks;
    }

    /**
     * Prints short summary about <code>Conference</code>. Something like ticket.
     * Contain <code>title, hall, duration, List<Talk></code>
     *
     * @return Formatted <code>String</code> with information.
     */
    @Override
    public String print() {
        String result = "*ABOUT*" + '\n';
        result = result + toString() + "\n";
        result = result + "Place: " + this.getPlace() + '\n';
        if (listOfTalks.isEmpty()) {
            result = result + "No talks available" + '\n';
        } else {
            result = result + "Talks: " + '\n';
            for (Talk talk : getListOfTalks()) {
                result = result + talk.print() + '\n';
            }
        }
        result = result + "***";
        return result;
    }

    @Override
    public String toString() {
        return "Conference: " + getTitle() + " (" + getDuration() + "min)";
    }
}
