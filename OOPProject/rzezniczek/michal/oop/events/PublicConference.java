/*
 * PublicConference.java
 * Version 1.0
 * Micha� Rze�niczek
 * Copyright (c) 2016. All rights reserved.
 */

package rzezniczek.michal.oop.events;

import rzezniczek.michal.oop.Hall;
import rzezniczek.michal.oop.exceptions.EventDoesNotExistException;
import rzezniczek.michal.oop.exceptions.SeatReservedException;
import rzezniczek.michal.oop.interfaces.IReservable;

import java.util.List;

/**
 * PublicConference is a class inherited from Conference class.
 * Contains the same elements, but you can reserve place on this event.
 * <ul>
 * <li>The title</li>
 * <li>The duration (min)</li>
 * <li>Place</li>
 * <li>List of Talk</li>
 * </ul>
 *
 * @author Micha� Rze�niczek
 * @version 1.0 2016
 * @see rzezniczek.michal.oop.events.Conference
 * @see rzezniczek.michal.oop.events.Talk
 */
public class PublicConference extends Conference implements IReservable {
    public PublicConference(String title, int duration, Hall place, List<Talk> talks) {
        super(title, duration, place, talks);
    }

    /**
     * Book a seats on movie.
     *
     * @param place seat's number to reserve.
     * @throws EventDoesNotExistException
     * @throws SeatReservedException
     */
    @Override
    public void book(int place) {
        try {
            getPlace().bookASeat(place, this);
        } catch (EventDoesNotExistException e) {
            e.getMessage();
        } catch (SeatReservedException e) {
            e.getMessage();
        }
    }

    /**
     * Displays free seats on standard output.
     */
    @Override
    public void getFreeSeats() {
        System.out.print("Free seats: ");
        if (getPlace().getFreeSeatsOn(this).size() != 0) {
            for (int i : getPlace().getFreeSeatsOn(this)) {
                System.out.print(i + " ");
            }
        } else {
            System.out.print("none");
        }

        System.out.println();
    }
}
