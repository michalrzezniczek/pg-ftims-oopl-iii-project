/*
 * Film.java
 * Version 1.0
 * Micha� Rze�niczek
 * Copyright (c) 2016. All rights reserved.
 */

package rzezniczek.michal.oop.events;

import rzezniczek.michal.oop.Hall;
import rzezniczek.michal.oop.exceptions.EventDoesNotExistException;
import rzezniczek.michal.oop.exceptions.SeatReservedException;
import rzezniczek.michal.oop.interfaces.IReservable;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Film is a class inherited from base class Event.
 * Contains basic information from Event and in addition Set of genres.
 * Contains:
 * <ul>
 * <li>The title</li>
 * <li>The duration (min)</li>
 * <li>Place</li>
 * <li>Set of genres</li>
 * </ul>
 *
 * @author Micha� Rze�niczek
 * @version 1.0 2016
 * @see rzezniczek.michal.oop.events.Event
 */
public class Film extends Event implements IReservable {
    private Set<String> genres = new HashSet<>();

    public Film(String title, int duration, Hall place, List<String> genres) {
        super(title, duration, place);
        this.genres.addAll(genres);
    }

    /**
     * Returns Set of genres.
     *
     * @return <code>Set<String></code>
     */
    public Set<String> getGenres() {
        return genres;
    }

    /**
     * Book a seats on movie.
     *
     * @param place seat's number to reserve.
     * @throws EventDoesNotExistException
     * @throws SeatReservedException
     */
    @Override
    public void book(int place) {
        try {
            getPlace().bookASeat(place, this);
        } catch (EventDoesNotExistException e) {
            e.getMessage();
        } catch (SeatReservedException e) {
            e.getMessage();
        }
    }

    /**
     * Displays free seats on standard output.
     */
    @Override
    public void getFreeSeats() {
        System.out.print("Free seats: ");
        if (getPlace().getFreeSeatsOn(this).size() != 0) {
            for (int i : getPlace().getFreeSeatsOn(this)) {
                System.out.print(i + " ");
            }
        } else {
            System.out.print("none");
        }

        System.out.println();
    }

    /**
     * Prints short summary about <code>Film</code>. Something like ticket.
     * Contain <code>title, hall, duration, list of genres</code>
     *
     * @return Formatted <code>String</code> with information.
     */
    @Override
    public String print() {
        String result = "*ABOUT*" + '\n';
        result = result + toString() + "\n";
        result = result + "Place: " + this.getPlace() + '\n';
        if (genres.isEmpty()) {
            result = result + "No genres available" + '\n';
        } else {
            result = result + "Genres: " + '\n';
            for (String genre : getGenres()) {
                result = result + genre + '\n';
            }
        }
        result = result + "***";
        return result;
    }

    @Override
    public String toString() {
        return "Film: " + getTitle() + " (" + getDuration() + "min)";
    }

}
