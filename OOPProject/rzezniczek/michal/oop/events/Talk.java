/*
 * Talk.java
 * Version 1.0
 * Micha� Rze�niczek
 * Copyright (c) 2016. All rights reserved.
 */

package rzezniczek.michal.oop.events;

import rzezniczek.michal.oop.interfaces.IPrintable;

/**
 * Talk is a base class.
 * A Talk object encapsulates the state information needed
 * to present talk.
 * Contains:
 * <ul>
 * <li>The title</li>
 * <li>Author</li>
 * </ul>
 *
 * @author Micha� Rze�niczek
 * @version 1.0 2016
 */
public class Talk implements IPrintable {
    private String title;
    private String author;

    public Talk(String title, String author) {
        this.title = title;
        this.author = author;
    }

    /**
     * Prints short summary about <code>Talk</code>.
     * Contain <code>title, author</code>
     *
     * @return Formatted <code>String</code> with information.
     */
    @Override
    public String print() {
        return '"' + title + '"' + " by " + author;
    }
}