/*
 * Event.java
 * Version 1.0
 * Micha� Rze�niczek
 * Copyright (c) 2016. All rights reserved.
 */

package rzezniczek.michal.oop.events;

import rzezniczek.michal.oop.Hall;
import rzezniczek.michal.oop.interfaces.IPrintable;
import rzezniczek.michal.oop.interfaces.IReservable;

/**
 * Base class which allow an application to manage different
 * types of Events.
 * A Event object encapsulates the state information needed
 * for managing different types of events.
 * Included information:
 * <ul>
 * <li>The title</li>
 * <li>The duration (min)</li>
 * <li>Place</li>
 * </ul>
 *
 * @author Micha� Rze�niczek
 * @version 1.0 2016
 */
public class Event implements IPrintable, IReservable {
    private String title;
    private int duration;
    private Hall place;

    protected Event(String title, int duration, Hall place) {
        this.title = title;
        this.duration = duration;
        this.place = place;
    }

    public String getTitle() {
        return title;
    }

    public int getDuration() {
        return duration;
    }

    public Hall getPlace() {
        return place;
    }

    /**
     * When you are trying to reserve unresolvable event.
     *
     * @param place seat's number to reserve.
     */
    @Override
    public void book(int place) {
        System.out.println("You cant book seat on closed event!");
    }

    /**
     * Displays free seats on standard output.
     */
    @Override
    public void getFreeSeats() {
        System.out.println("None");
    }

    /**
     * Prints short summary about <code>Event</code>. Something like ticket.
     * Contain <code>title, duration, hall</code>
     *
     * @return Formatted <code>String</code> with information.
     */
    public String print() {
        String result = "*ABOUT*" + '\n';
        result = result + toString() + "\n";
        result = result + "Place: " + place.toString();
        result = result + "***";
        return result;
    }

    @Override
    public String toString() {
        return "Event: " + title + " (" + duration + "min)";
    }

}
