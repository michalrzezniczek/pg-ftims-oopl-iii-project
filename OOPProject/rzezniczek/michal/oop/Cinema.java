/*
 * Cinema.java
 * Version 1.0
 * Micha� Rze�niczek
 * Copyright (c) 2016. All rights reserved.
 */

package rzezniczek.michal.oop;

import rzezniczek.michal.oop.events.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Base class which allow an application to create object similar to real cinema.
 * Included information:
 * <ul>
 * <li>List of hall.</li>
 * <li>List of events in cinema.</li>
 * </ul>
 *
 * @author Micha� Rze�niczek
 * @version 1.0 2016
 */
public class Cinema {
    private List<Hall> hallList;
    private List<Event> events;

    public Cinema(List<Integer> halls) {
        hallList = new ArrayList<>();
        events = new ArrayList<>();

        for (int i : halls) {
            hallList.add(new Hall(i));
        }
    }

    /**
     * Returns list of Events in cinema.
     *
     * @return <code>List<Event></code> which are added to cinema.
     */
    public List<Event> getEventsInCinema() {
        return events;
    }

    /**
     * Returns list of Hall in cinema.
     *
     * @return <code>List<Hall></code> which are added to cinema.
     */
    public List<Hall> getHallList() {
        return hallList;
    }

    /**
     * Creates movie.
     *
     * @param title      The title of movie.
     * @param duration   The duration time in minutes.
     * @param hallNumber - The number of hall. (Hall must be right!)
     * @param genres     - List of genres.
     * @see rzezniczek.michal.oop.events.Film
     */
    public void addFilm(String title, int duration, int hallNumber, List<String> genres) {
        Event film = new Film(title, duration, hallList.get(hallNumber - 1), genres);
        hallList.get(hallNumber - 1).addEvent(film);
        events.add(film);
    }

    /**
     * Creates Conference.
     *
     * @param title      The title of conference.
     * @param duration   The duration time in minutes.
     * @param hallNumber - The number of hall. (Hall must be right!)
     * @param talks      - List of <code>Talk</code>.
     * @see rzezniczek.michal.oop.events.Conference
     * @see rzezniczek.michal.oop.events.Talk
     */
    public void addConference(String title, int duration, int hallNumber, List<Talk> talks) {
        Event conference = new Conference(title, duration, hallList.get(hallNumber - 1), talks);
        hallList.get(hallNumber - 1).addEvent(conference);
        events.add(conference);
    }

    /**
     * Creates public conference.
     *
     * @param title      The title of conference.
     * @param duration   The duration time in minutes.
     * @param hallNumber - The number of hall. (Hall must be right!)
     * @param talks      - List of <code>Talk</code>.
     * @see rzezniczek.michal.oop.events.PublicConference
     * @see rzezniczek.michal.oop.events.Talk
     */
    public void addPublicConference(String title, int duration, int hallNumber, List<Talk> talks) {
        Event publicConf = new PublicConference(title, duration, hallList.get(hallNumber - 1), talks);
        hallList.get(hallNumber - 1).addEvent(publicConf);
        events.add(publicConf);
    }
}