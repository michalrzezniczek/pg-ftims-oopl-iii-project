/*
 * EventDoesNotExistException.java
 * Version 1.0
 * Micha� Rze�niczek
 * Copyright (c) 2016. All rights reserved.
 */

package rzezniczek.michal.oop.exceptions;

/**
 * Exception class.
 * Use it when <code>Event</code> doesn't exists.
 *
 * @author Micha� Rze�niczek
 * @version 1.0 2016
 */
public class EventDoesNotExistException extends Exception {

    @Override
    public String getMessage() {
        return "Event does not exist! Try again later.";
    }
}
