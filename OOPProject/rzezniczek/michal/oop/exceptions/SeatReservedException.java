/*
 * SeatReservedException.java
 * Version 1.0
 * Micha� Rze�niczek
 * Copyright (c) 2016. All rights reserved.
 */

package rzezniczek.michal.oop.exceptions;

/**
 * Exception class.
 * Use it when seat is already booked.
 *
 * @author Micha� Rze�niczek
 * @version 1.0 2016
 */
public class SeatReservedException extends Exception {

    @Override
    public String getMessage() {
        return "This seat is reserved. Choose again.";
    }
}
