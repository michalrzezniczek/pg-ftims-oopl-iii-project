/*
 * IReservable.java
 * Version 1.0
 * Micha� Rze�niczek
 * Copyright (c) 2016. All rights reserved.
 */

package rzezniczek.michal.oop.interfaces;

/**
 * Class description.
 *
 * @author Micha� Rze�niczek
 * @version 1.0 2016
 */
public interface IReservable {
    /**
     * Use to reserve a seat.
     *
     * @param place seat's number to reserve.
     */
    void book(int place);

    /**
     * Displays free seats on standard output.
     */
    void getFreeSeats();
}
