/*
 * IPrintable.java
 * Version 1.0
 * Micha� Rze�niczek
 * Copyright (c) 2016. All rights reserved.
 */

package rzezniczek.michal.oop.interfaces;

/**
 * Class description.
 *
 * @author Micha� Rze�niczek
 * @version 1.0 2016
 */
public interface IPrintable {
    /**
     * Create short summary about Object.
     *
     * @return String
     */
    String print();
}
