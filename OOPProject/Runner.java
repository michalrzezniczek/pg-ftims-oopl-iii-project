/*
 * Runner.java
 * Version 1.0
 * Micha� Rze�niczek
 * Copyright (c) 2016. All rights reserved.
 */

import rzezniczek.michal.oop.*;
import rzezniczek.michal.oop.events.*;
import rzezniczek.michal.oop.interfaces.IPrintable;
import rzezniczek.michal.oop.interfaces.IReservable;


import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Class Runner responsible for starting application.
 *
 * @author Micha� Rze�niczek
 * @version 1.0 2016
 */
public class Runner {
    private static Scanner scanner = new Scanner(System.in);
    private static Cinema cinema;

    public static void main(String[] args) {

        /* Demo data which shows how application works.*/
        List<String> emptyGenresList = new ArrayList<>();
        List<String> fewGenres = new ArrayList<>();
        fewGenres.add("Drama");
        fewGenres.add("Sci-Fi");

        List<Talk> emptyTalkList = new ArrayList<>();
        List<Talk> fewTalkList = new ArrayList<>();
        Talk talk = new Talk("How to", "Mike R.");
        fewTalkList.add(talk);
        talk = new Talk("Creating tea", "Joseph No");
        fewTalkList.add(talk);

        List<Integer> capacityOfHalls = new ArrayList<>();
        capacityOfHalls.add(2);
        capacityOfHalls.add(15);

        cinema = new Cinema(capacityOfHalls);

        cinema.addFilm("Empty genres.", 100, 1, emptyGenresList);
        cinema.addFilm("Interstellar", 169, 1, fewGenres);

        cinema.addConference("Private event.", 100, 2, emptyTalkList);
        cinema.addPublicConference("Conference of tutorials.", 60, 2, fewTalkList);

        for(IPrintable e : cinema.getEventsInCinema()){
            System.out.println(e.print());
        }

        System.out.println(cinema.getEventsInCinema().get(0).getPlace().getFreeSeatsOn(cinema.getEventsInCinema().get(0)));
        cinema.getEventsInCinema().get(0).book(1);
        cinema.getEventsInCinema().get(0).book(2);
        System.out.println(cinema.getHallList().get(0).getFreeSeatsOn(cinema.getEventsInCinema().get(0)));
        cinema.getEventsInCinema().get(0).book(2);
        System.out.println(cinema.getEventsInCinema().get(0).getPlace().getFreeSeatsOn(cinema.getEventsInCinema().get(0)));



        System.out.println("\n\n\n");
        System.out.println("Do you want to use demo data? Y/N");
        if(scanner.nextLine().equals("N")){
            capacityOfHalls.clear();
            System.out.println("Write capacity of each hall. One capacity in one line. Capacity equals 0 => end of creating cinema's halls");
            int capacity = scanner.nextInt();
            while (capacity != 0){
                capacityOfHalls.add(capacity);
                capacity = scanner.nextInt();
            }

            cinema = new Cinema(capacityOfHalls);
        }


        /*
        * Main part of Console Application.
        * Can work on demo data or new data.
        */
        boolean exit = false;

        while(!exit){
            System.out.println("\n--");
            printMenu();
            switch (scanner.nextInt()){
                case 1:
                    addFilm();
                    break;
                case 2:
                    addConference(false);
                    break;
                case 3:
                    addConference(true);
                    break;
                case 4:
                    printEventsInCinema();
                    break;
                case 5:
                    addReservation();
                    break;
                case 6:
                    exit = true;
                    break;
                default:
                    System.out.println("Wrong input. Try again.");
                    break;
            }
        }
    }

    private static void printMenu(){
        System.out.println("1 - Add Film");
        System.out.println("2 - Add Conference");
        System.out.println("3 - Add Public Conference");
        System.out.println("4 - Get events list in cinema");
        System.out.println("5 - Create reservation.");
        System.out.println("6 - Exit");
    }
    private static void printEventsInCinema() {
        int i = 0;
        for(IPrintable e : cinema.getEventsInCinema()){
            System.out.println(i + " - " + e.print());
            i++;
        }
    }
    private static void printHalls(){
        int i = 1;
        for(IPrintable h : cinema.getHallList()){
            System.out.println(i + " - " + h.print());
            i++;
        }
    }

    private static void addFilm(){

        String title;
        int duration;
        int hallNumber;
        List<String> genres;

        System.out.println("---ADD FILM---");
        System.out.println("Enter title.");
        scanner.nextLine();
        title = scanner.nextLine();
        System.out.println("Enter duration.");
        duration = scanner.nextInt();
        System.out.println("Choose number of hall.");
        printHalls();
        hallNumber = scanner.nextInt();
        System.out.println("Enter genres. (One per line, to end enter empty line");
        genres = collectingGenres();

        cinema.addFilm(title, duration, hallNumber, genres);
    }
    private static void addConference(boolean isPublic) {
        String title;
        int duration;
        int hallNumber;
        List<Talk> talkList;
        scanner.nextLine();

        System.out.print("--ADD ");
        if (isPublic) {
            System.out.print("Public ");
        }
        System.out.println("Conference---");
        System.out.println("Enter title.");
        title = scanner.nextLine();
        System.out.println("Enter duration.");
        duration = scanner.nextInt();
        System.out.println("Choose number of hall.");
        printHalls();
        hallNumber = scanner.nextInt();
        System.out.println("Enter talks. To stop enter empty line");
        talkList = collectTalkList();

        if (isPublic) {
            cinema.addPublicConference(title, duration, hallNumber, talkList);
        } else {
            cinema.addConference(title, duration, hallNumber, talkList);
        }
    }
    private static void addReservation() {
        Event event;
        IReservable eventReserve;
        scanner.nextLine();

        printEventsInCinema();
        System.out.println("Choose number of event");
        event = cinema.getEventsInCinema().get(scanner.nextInt());
        eventReserve = event;
        if(event.getClass() != Conference.class) {
            if (event.getPlace().getFreeSeatsOn(event) == null) {
                System.out.println("I'm sorry. There's no free seats. Choose another event.");
            } else {
                System.out.println("Do you wanna see which seats are free? Y/N");
                if (scanner.next().equals("Y")) {
                    //System.out.println(event.getPlace().getFreeSeatsOn(event));
                    eventReserve.getFreeSeats();
                }

                System.out.println("Which seat do you want to book?");
                eventReserve.book(scanner.nextInt());

                System.out.println(event.print());
            }
        }
        else{
            System.out.println("This is a closed event.");
        }
    }

    private static List<Talk> collectTalkList() {
        String title;
        String author;
        List<Talk> talkList = new ArrayList<>();
        scanner.nextLine();

        System.out.println("Enter title of talk");
        title = scanner.nextLine();
        System.out.println("Enter author of talk");
        author = scanner.nextLine();
        while(!author.equals("")){
            talkList.add(new Talk(title, author));
            System.out.println("Enter title of talk");
            title = scanner.nextLine();
            System.out.println("Enter author of talk");
            author = scanner.nextLine();
        }

        return talkList;
    }
    private static List<String> collectingGenres() {
        String genre;
        List<String> genres = new ArrayList<>();
        scanner.nextLine();

        genre = scanner.nextLine();
        while(!genre.equals("")){
            genres.add(genre);

            genre = scanner.nextLine();
        }

        return genres;
    }
}