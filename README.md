# Object-Oriented Programming Languages III final project #

##Author##
Michał Rzeźniczek
##Description##
* Simple Cinema Booking System.
* You can:
	1. Add movie/conference.
	2. Reserve a seat.
	3. Create a cinema with halls (on the beginging).

##Instructions##
* Java 1.7
* How to compile
	1. Add entire contents of a ***OOPProject*** folder (with source codes) to project in IDE.
	2. Let the IDE compile for you.
* How to run application
	1. Run ***cmd.exe***.
	2. Go to dir with ***RUNME.jar***.
	3. Enter into console ***java -jar RUNME.jar***